package com.example.appmicalculadora

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class operacionesActivity : AppCompatActivity() {
    private lateinit var txtUsuario: TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var txtResultado: TextView

    private lateinit var btnSumar: Button
    private lateinit var btnRestar: Button
    private lateinit var btnMultiplicar: Button
    private lateinit var btnDividir: Button

    private lateinit var btnCerrar: Button
    private lateinit var btnLimpiar: Button

    private lateinit var operaciones: Operaciones

    var opcion: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operaciones)
        enableEdgeToEdge()
        iniciarComponentes()
        eventosClic()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    private fun iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario)
        txtResultado = findViewById(R.id.txtResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)
        btnCerrar = findViewById(R.id.btnCerrar)
        btnLimpiar = findViewById(R.id.btnLimpiar)

        val bundle: Bundle? = intent.extras
        txtUsuario.text = bundle?.getString("usuario")
    }

    private fun validar(): Boolean {
        return !(txtNum1.text.toString().isEmpty() || txtNum2.text.toString().isEmpty())
    }

    private fun operaciones(): Float {
        var num1: Float = 0f
        var num2: Float = 0f
        var res: Float = 0f

        if (validar()) {
            num1 = txtNum1.text.toString().toFloat()
            num2 = txtNum2.text.toString().toFloat()
            operaciones = Operaciones(num1, num2)
            when (opcion) {
                1 -> res = operaciones.sumar()
                2 -> res = operaciones.restar()
                3 -> res = operaciones.multiplicar()
                4 -> res = operaciones.dividir()
            }
        } else {
            Toast.makeText(this, "Faltaron datos por capturar", Toast.LENGTH_SHORT).show()
        }

        return res
    }

    private fun eventosClic() {
        btnSumar.setOnClickListener {
            opcion = 1
            txtResultado.text = operaciones().toString()
        }

        btnRestar.setOnClickListener {
            opcion = 2
            txtResultado.text = operaciones().toString()
        }

        btnMultiplicar.setOnClickListener {
            opcion = 3
            txtResultado.text = operaciones().toString()
        }

        btnDividir.setOnClickListener {
            if (validar()) {
                val num2 = txtNum2.text.toString().toFloat()
                if (num2 == 0f) {
                    Toast.makeText(this, "No se puede dividir por cero", Toast.LENGTH_SHORT).show()
                } else {
                    opcion = 4
                    txtResultado.text = operaciones().toString()
                }
            } else {
                Toast.makeText(this, "Faltaron datos por capturar", Toast.LENGTH_SHORT).show()
            }
        }

        btnLimpiar.setOnClickListener {
            txtResultado.text = ""
            txtNum2.text.clear()
            txtNum1.text.clear()
        }

        btnCerrar.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage("¿Deseas cerrar la app?")
            builder.setPositiveButton(android.R.string.yes) { _, _ -> finish() }
            builder.setNegativeButton(android.R.string.no, null)
            builder.show()
        }
    }


}




